# Create the Highly Available Database Cluster using Galera and HAProxy
module "sippin_db" {
  source           = "bitbucket.org/netloudev/galera_cluster.git"
  project          = "${var.project}"
  lb_size          = "${var.node_size}"
  db_node_size     = "${var.node_size}"
  image_slug       = "${var.image_slug}"
  region           = "${var.region}"
  keys             = "${var.keys}"
  private_key_path = "${var.private_key_path}"
  ssh_fingerprint  = "${var.ssh_fingerprint}"
  public_key       = "${var.public_key}"
  ansible_user     = "${var.ansible_user}"
}
